package ru.terrakok.gitlabclient.system.message

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow

/**
 * Created by Konstantin Tskhovrebov (aka @terrakok) on 03.09.18.
 */
class SystemMessageNotifier {
    private val flow = MutableSharedFlow<SystemMessage>()

    val notifier: Flow<SystemMessage> = flow
    fun send(message: SystemMessage) = flow.tryEmit(message)
    fun send(message: String) = flow.tryEmit(SystemMessage(message))
}
